﻿using ConsoleParser.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using MySql.Data.MySqlClient;
using System.Data.Entity;

namespace ConsoleParserTest.Extensions
{
    public class DatabaseFixture : IDisposable
    {

        public const string connString = "server=localhost;port=3306;database=testdb;uid=testdb;password=testdb";
        public DatabaseFixture()
        {
            var connection = new MySqlConnection(connString);
            
            Repository = new ParserRepository(connection, true);
                
        }


        public ParserRepository Repository { get; private set; }

        public void Dispose()
        {
            if(Repository != null )
            Repository.Dispose();
        }
    }
}
