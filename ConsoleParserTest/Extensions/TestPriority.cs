﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace ConsoleParserTest.Extensions
{
  /*  class PrioritizedFixtureAttribute : RunWithAttribute
    {
        public PrioritizedFixtureAttribute()
            : base(typeof(PrioritizedFixtureClassCommand))
        {
        }
    }

    class PrioritizedFixtureClassCommand : ITestClassCommand
    {
        // The default implementation.
        // Assume members not shown simply delegate to this
        readonly TestClassCommand _inner = new TestClassCommand();

        public int ChooseNextTest(ICollection<IMethodInfo> testsLeftToRun)
        {
            return 0;
        }

        public IEnumerable<IMethodInfo> EnumerateTestMethods()
        {
            return from m in _inner.EnumerateTestMethods()
                   let priority = GetPriority(m)
                   orderby priority
                   select m;
        }

        private static int GetPriority(IMethodInfo method)
        {
            var priorityAttribute = method
                .GetCustomAttributes(typeof(TestPriorityAttribute))
                .FirstOrDefault();

            return priorityAttribute == null
                ? 0
                : priorityAttribute.GetPropertyValue<int>("Priority");
        }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    class TestPriorityAttribute : Attribute
    {
        readonly int _priority;

        public TestPriorityAttribute(int priority)
        {
            _priority = priority;
        }

        public int Priority
        {
            get { return _priority; }
        }
    }*/
}
