﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace ConsoleParserTest.Extensions
{
    public class ParserRepositoryTestCaseOrderer : ITestCaseOrderer
    {
        private IMessageSink m_sink;
        public ParserRepositoryTestCaseOrderer(IMessageSink messageSink)
        {
            m_sink = messageSink;
        }
        public IEnumerable<TTestCase> OrderTestCases<TTestCase>(IEnumerable<TTestCase> testCases) where TTestCase : Xunit.Abstractions.ITestCase
        {
            var sortedLst = new SortedDictionary<int, List<TTestCase>>();
            var tempLst = new List<TTestCase>();

            int lowestKey = 0;

            foreach(var testCase in testCases)
            {
                
                var attr = testCase.TestMethod.Method.GetCustomAttributes(typeof(OrderAttribute).AssemblyQualifiedName).FirstOrDefault();

                if (attr == null)
                    tempLst.Add(testCase);
                else
                {
                    var order = attr.GetNamedArgument<int>("Rank");
                    GetOrCreate(sortedLst, order).Add(testCase);

                    lowestKey = System.Math.Max(lowestKey, order);
                }
            }

            if(tempLst.Count > 0)
            {
                var cnt = lowestKey + lowestKey == int.MaxValue ? 0 : 1;
                IList<TTestCase> tmpCases = null;

                foreach(var testcase in tempLst)
                {
                    if (tmpCases == null)
                        tmpCases = GetOrCreate(sortedLst, cnt);

                    tmpCases.Add(testcase);                    
                }
            }


            foreach(var lst in sortedLst.Keys.Select(key => sortedLst[key]))
            {
                lst.Sort((frst, scnd) => StringComparer.OrdinalIgnoreCase.Compare(frst.TestMethod.Method.Name, scnd.TestMethod.Method.Name));
                foreach (TTestCase test in lst)
                    yield return test;
            }               

        }

        public static TVALUE GetOrCreate<TVALUE, TKEY>(IDictionary<TKEY, TVALUE> sortedLst, TKEY order) where TVALUE : new()
        {
            TVALUE lst;

            if (sortedLst.TryGetValue(order, out lst))
                return lst;

            lst = new TVALUE();
            sortedLst.Add(order, lst);

            return lst;
        }
    }


    [Serializable]
    public class DiagnosticMessage: IDiagnosticMessage
    {
        public DiagnosticMessage(ITestCase message)
        {
            Message = message.DisplayName + " " ;
        }

        public string Message
        {
            get;
            set;
        }
        
    }
}
