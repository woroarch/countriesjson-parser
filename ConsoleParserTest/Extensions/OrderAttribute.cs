﻿using System;

namespace ConsoleParserTest.Extensions
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple=false)]
    public class OrderAttribute: Attribute
    {
        public OrderAttribute(int rank)
        {
            Rank = rank;
        }

        public Int32 Rank { get; private set; }
    }
}
