﻿using ConsoleParser.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace ConsoleParserTest
{
    public class UtilitiesHelpers
    {
        private ITestOutputHelper m_outputHelper;
        public UtilitiesHelpers(ITestOutputHelper output)
        {
            m_outputHelper = output;
        }

        [Fact]
        public void GetContinentsName_NoParameter_ContainsNorthAndSouth_America_WithSpace()
        {
            var continentsName = Helpers.GetContinentsName();

            foreach (var name in continentsName)
                m_outputHelper.WriteLine(name);

            Assert.Contains<string>("North America", continentsName);
            Assert.Contains<string>("South America", continentsName);

        }
    }
}
