﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleParser.Services;
using Moq;
using Xunit;
using System.Data.Entity;
using ConsoleParser.Models;
using ConsoleParser.Repository;
using Newtonsoft.Json;
using GeoJSON.Net.Feature;
using Newtonsoft.Json.Serialization;
using System.IO;

namespace ConsoleParserTest
{
    [TestCaseOrderer("ConsoleParserTest.Extensions.ParserRepositoryTestCaseOrderer", "ConsoleParserTest")]
    public class ParserServiceFacts
    {

        [Fact]
        public void AddOrUpdateRegion_SingleValidRegion_should_succeed()
        {

            // set up repository Mock
            var mockSet = new Mock<DbSet<Region>>();

            var mockContext = new Mock<ParserRepository>();

            mockContext.Setup(m => m.Regions).Returns(mockSet.Object);

            //pass in mock to service internal ctr

            var svc = new ParserService(mockContext.Object);
            //Act
            svc.AddRegion(new Region { Continent = Continents.Africa, Name = "Algeria" });
            //Assert
            mockSet.Verify(m => m.Add(It.IsAny<Region>()), Times.Once);
            mockContext.Verify(m => m.SaveChanges(), Times.Once);
        }

        [Fact]
        public void AddOrUpdateCountry_SingleValidCountry_should_succeed()
        {

            // set up repository Mock
            var mockSet = new Mock<DbSet<Country>>();

            var mockContext = new Mock<ParserRepository>();

            mockContext.Setup(m => m.Countries).Returns(mockSet.Object);

            //pass in mock to service internal ctr
            var svc = new ParserService(mockContext.Object);

            //Act
            /*
            "name": "Afghanistan",
    "name_fr": "Afghanistan",
    "ISO3166-1-Alpha-2": "AF",
    "ISO3166-1-Alpha-3": "AFG",
    "ISO3166-1-numeric": "004",
    "ITU": "AFG",
    "MARC": "af",
    "WMO": "AF",
    "DS": "AFG",
    "Dial": "93",
    "FIFA": "AFG",
    "FIPS": "AF",
    "GAUL": "1",
    "IOC": "AFG",
    "currency_alphabetic_code": "AFN",
    "currency_country_name": "AFGHANISTAN",
    "currency_minor_unit": "2",
    "currency_name": "Afghani",
    "currency_numeric_code": "971",
    "is_independent": "Yes"
             */

            svc.AddCountry(new Country
            {
                Name = "Afghanistan",
                NameFrench = "Afghanistan",
                NameISOAlpha2 = "AF",
                NameISOAlpha3 = "AFG",
                NameISONumeric = 004,
                ITU = "AFG",
                MARC = "af",
                WMO = "AF",
                DS = "AFG",
                Dial = "93",
                FIFA = "AFG",
                FIPS = "AF",
                GAUL = "1",
                IOC = "AFG",
                CurrencyAlphaCode = "AFN",
                CurrencyCountryName = "AFGHANISTAN",
                CurrencyUnit = 2,
                CurrencyName = "Afghani",
                CurrencyCode = 971,
                IsIndependent = true
            });

            //Assert
            mockSet.Verify(m => m.Add(It.IsAny<Country>()), Times.Once);
            mockContext.Verify(m => m.SaveChanges(), Times.Once);
        }


        [Fact]
        public void AddOrUpdateCountryRange_MultipleValidCountries_succeed()
        {


            // set up repository Mock
            var mockSet = new Mock<DbSet<Country>>();

            var mockContext = new Mock<ParserRepository>();

            mockContext.Setup(m => m.Countries).Returns(mockSet.Object);

            //pass in mock to service internal ctr
            var svc = new ParserService(mockContext.Object);

            //Act
            /*
            "name": "Afghanistan",
    "name_fr": "Afghanistan",
    "ISO3166-1-Alpha-2": "AF",
    "ISO3166-1-Alpha-3": "AFG",
    "ISO3166-1-numeric": "004",
    "ITU": "AFG",
    "MARC": "af",
    "WMO": "AF",
    "DS": "AFG",
    "Dial": "93",
    "FIFA": "AFG",
    "FIPS": "AF",
    "GAUL": "1",
    "IOC": "AFG",
    "currency_alphabetic_code": "AFN",
    "currency_country_name": "AFGHANISTAN",
    "currency_minor_unit": "2",
    "currency_name": "Afghani",
    "currency_numeric_code": "971",
    "is_independent": "Yes"
             */

            svc.AddCountries(new List<Country>{new Country
            {
                Name = "Afghanistan",
                NameFrench = "Afghanistan",
                NameISOAlpha2 = "AF",
                NameISOAlpha3 = "AFG",
                NameISONumeric = 004,
                ITU = "AFG",
                MARC = "af",
                WMO = "AF",
                DS = "AFG",
                Dial = "93",
                FIFA = "AFG",
                FIPS = "AF",
                GAUL = "1",
                IOC = "AFG",
                CurrencyAlphaCode = "AFN",
                CurrencyCountryName = "AFGHANISTAN",
                CurrencyUnit = 2,
                CurrencyName = "Afghani",
                CurrencyCode = 971,
                IsIndependent = true
            },
            new Country
            {
                Name = "Afghanistan",
                NameFrench = "Afghanistan",
                NameISOAlpha2 = "AF",
                NameISOAlpha3 = "AFG",
                NameISONumeric = 004,
                ITU = "AFG",
                MARC = "af",
                WMO = "AF",
                DS = "AFG",
                Dial = "93",
                FIFA = "AFG",
                FIPS = "AF",
                GAUL = "1",
                IOC = "AFG",
                CurrencyAlphaCode = "AFN",
                CurrencyCountryName = "AFGHANISTAN",
                CurrencyUnit = 2,
                CurrencyName = "Afghani",
                CurrencyCode = 971,
                IsIndependent = true
            },
            new Country
            {
                Name = "Afghanistan",
                NameFrench = "Afghanistan",
                NameISOAlpha2 = "AF",
                NameISOAlpha3 = "AFG",
                NameISONumeric = 004,
                ITU = "AFG",
                MARC = "af",
                WMO = "AF",
                DS = "AFG",
                Dial = "93",
                FIFA = "AFG",
                FIPS = "AF",
                GAUL = "1",
                IOC = "AFG",
                CurrencyAlphaCode = "AFN",
                CurrencyCountryName = "AFGHANISTAN",
                CurrencyUnit = 2,
                CurrencyName = "Afghani",
                CurrencyCode = 971,
                IsIndependent = true
            }
        });

            //Assert
            //mockContext.VerifySet(m => m.Configuration.AutoDetectChangesEnabled = false);
            mockSet.Verify(m => m.AddRange(It.IsAny<IEnumerable<Country>>()), Times.Once);
            mockContext.Verify(m => m.SaveChanges(), Times.Once);
        }

        [Fact]
        public void ParseRegionJson_StringStream_succeed()
        {

            var regions = ParserService.ParseRegionJson(GetCountriesGeojson());

            var frst = regions.FirstOrDefault();

            Assert.NotNull(regions);
            Assert.NotNull(frst);
            Assert.Equal(1, regions.Count());
            Assert.Equal(frst.Name, "Southern Asia");
            Assert.Equal(frst.Continent, Continents.Asia);

        }



        [Fact]
        public void ParseRegionsJson_FileStream_succeed()
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            var filePath = Path.Combine(dir, "Data", "countries.geojson");
            var regions = ParserService.ParseRegionJson(File.ReadAllText(filePath));

            var frst = regions.FirstOrDefault();

            Assert.NotNull(regions);
            Assert.NotNull(frst);
            Assert.InRange(regions.Count(), 2, 100);
            Assert.Equal(frst.Name, "Southern Asia");
            Assert.Equal(frst.Continent, Continents.Asia);
        }

        [Fact]
        public void ParseCountryJson_StringStream_succeed()
        {
            var countries = ParserService.ParseCountriesJson(GetCountriesCode());

            var frst = countries.FirstOrDefault();

            Assert.NotNull(countries);
            Assert.NotNull(frst);
            Assert.Equal(2, countries.Count());
            Assert.Equal(frst.GAUL, "1");
            Assert.Equal(frst.IOC, "AFG");
        }

        [Fact]
        public void ParseCountriesJson_FileStream_succeed()
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            var filePath = Path.Combine(dir, "Data", "country-codes.json");
            var countries = ParserService.ParseCountriesJson(File.ReadAllText(filePath));

            var frst = countries.FirstOrDefault();

            Assert.NotNull(countries);
            Assert.NotNull(frst);
            Assert.InRange(countries.Count(), 100, 300);
            Assert.Equal(frst.GAUL, "1");
            Assert.Equal(frst.IOC, "AFG");
        }

        internal static string GetCountriesGeojson()
        {
            var countriesgeoJson = @" {
                'type': 'FeatureCollection',
                                                                                
                'features': [
                { 'type': 'Feature', 'properties': { 'scalerank': 1, 'featurecla': 'Admin-0 country', 'labelrank': 3.0, 'sovereignt': 'Afghanistan', 'sov_a3': 'AFG', 'adm0_dif': 0.0, 'level': 2.0, 'type': 'Sovereign country', 'admin': 'Afghanistan', 'adm0_a3': 'AFG', 'geou_dif': 0.0, 'geounit': 'Afghanistan', 'gu_a3': 'AFG', 'su_dif': 0.0, 'subunit': 'Afghanistan', 'su_a3': 'AFG', 'brk_diff': 0.0, 'name': 'Afghanistan', 'name_long': 'Afghanistan', 'brk_a3': 'AFG', 'brk_name': 'Afghanistan', 'brk_group': null, 'abbrev': 'Afg.', 'postal': 'AF', 'formal_en': 'Islamic State of Afghanistan', 'formal_fr': null, 'note_adm0': null, 'note_brk': null, 'name_sort': 'Afghanistan', 'name_alt': null, 'mapcolor7': 5.0, 'mapcolor8': 6.0, 'mapcolor9': 8.0, 'mapcolor13': 7.0, 'pop_est': 28400000.0, 'gdp_md_est': 22270.0, 'pop_year': -99.0, 'lastcensus': 1979.0, 'gdp_year': -99.0, 'economy': '7. Least developed region', 'income_grp': '5. Low income', 'wikipedia': -99.0, 'fips_10': null, 'iso_a2': 'AF', 'iso_a3': 'AFG', 'iso_n3': '004', 'un_a3': '004', 'wb_a2': 'AF', 'wb_a3': 'AFG', 'woe_id': -99.0, 'adm0_a3_is': 'AFG', 'adm0_a3_us': 'AFG', 'adm0_a3_un': -99.0, 'adm0_a3_wb': -99.0, 'continent': 'Asia', 'region_un': 'Asia', 'subregion': 'Southern Asia', 'region_wb': 'South Asia', 'name_len': 11.0, 'long_len': 11.0, 'abbrev_len': 4.0, 'tiny': -99.0, 'homepart': 1.0 }, 'geometry': { 'type': 'Polygon', 'coordinates': [ [ [ 61.210817091725744, 35.650072333309225 ], [ 62.230651483005886, 35.270663967422294 ], [ 62.98466230657661, 35.404040839167621 ], [ 63.193538445900352, 35.857165635718914 ], [ 63.98289594915871, 36.007957465146603 ], [ 64.546479119733903, 36.312073269184268 ], [ 64.746105177677407, 37.111817735333304 ], [ 65.588947788357842, 37.305216783185642 ], [ 65.745630731066825, 37.661164048812068 ], [ 66.217384881459338, 37.39379018813392 ], [ 66.51860680528867, 37.362784328758792 ], [ 67.075782098259623, 37.356143907209287 ], [ 67.829999627559516, 37.144994004864685 ], [ 68.135562371701383, 37.023115139304309 ], [ 68.859445835245936, 37.344335842430596 ], [ 69.196272820924378, 37.151143500307427 ], [ 69.518785434857961, 37.60899669041342 ], [ 70.116578403610333, 37.588222764632093 ], [ 70.270574171840138, 37.735164699854025 ], [ 70.376304152309302, 38.138395901027522 ], [ 70.806820509732887, 38.486281643216415 ], [ 71.348131137990265, 38.258905341132163 ], [ 71.239403924448169, 37.953265082341886 ], [ 71.541917759084782, 37.905774441065645 ], [ 71.448693475230243, 37.06564484308052 ], [ 71.844638299450594, 36.738171291646921 ], [ 72.193040805962397, 36.948287665345674 ], [ 72.636889682917285, 37.047558091778356 ], [ 73.260055779925011, 37.495256862939002 ], [ 73.9486959166465, 37.4215662704908 ], [ 74.980002475895418, 37.419990139305895 ], [ 75.158027785140916, 37.133030910789117 ], [ 74.575892775372978, 37.020841376283457 ], [ 74.067551710917826, 36.836175645488453 ], [ 72.920024855444467, 36.720007025696319 ], [ 71.846291945283923, 36.509942328429858 ], [ 71.26234826038575, 36.074387518857804 ], [ 71.498767938121091, 35.650563259416003 ], [ 71.613076206350712, 35.153203436822864 ], [ 71.115018751921639, 34.733125718722235 ], [ 71.156773309213463, 34.348911444632151 ], [ 70.881803012988399, 33.98885590263852 ], [ 69.930543247359594, 34.02012014417511 ], [ 70.323594191371598, 33.358532619758392 ], [ 69.687147251264861, 33.105498969041236 ], [ 69.262522007122556, 32.5019440780883 ], [ 69.317764113242561, 31.901412258424443 ], [ 68.926676873657669, 31.620189113892067 ], [ 68.556932000609322, 31.713310044882018 ], [ 67.792689243444784, 31.582930406209631 ], [ 67.683393589147471, 31.303154201781421 ], [ 66.938891229118468, 31.304911200479353 ], [ 66.381457553986024, 30.738899237586452 ], [ 66.346472609324422, 29.887943427036177 ], [ 65.046862013616106, 29.472180691031905 ], [ 64.350418735618518, 29.560030625928093 ], [ 64.148002150331251, 29.340819200145972 ], [ 63.550260858011171, 29.468330796826166 ], [ 62.549856805272782, 29.318572496044311 ], [ 60.874248488208792, 29.829238999952608 ], [ 61.781221551363444, 30.735850328081238 ], [ 61.699314406180832, 31.379506130492672 ], [ 60.941944614511129, 31.548074652628753 ], [ 60.863654819588966, 32.182919623334428 ], [ 60.536077915290775, 32.981268825811568 ], [ 60.963700392506006, 33.528832302376259 ], [ 60.528429803311582, 33.676446031218006 ], [ 60.803193393807447, 34.404101874319863 ], [ 61.210817091725744, 35.650072333309225 ] ] ] } }
                ]}";

            return countriesgeoJson;
        }

        internal static string GetCountriesCode()
        {
            var codes = @"[
              {
                'name': 'Afghanistan',
                'name_fr': 'Afghanistan',
                'ISO3166-1-Alpha-2': 'AF',
                'ISO3166-1-Alpha-3': 'AFG',
                'ISO3166-1-numeric': '004',
                'ITU': 'AFG',
                'MARC': 'af',
                'WMO': 'AF',
                'DS': 'AFG',
                'Dial': '93',
                'FIFA': 'AFG',
                'FIPS': 'AF',
                'GAUL': '1',
                'IOC': 'AFG',
                'currency_alphabetic_code': 'AFN',
                'currency_country_name': 'AFGHANISTAN',
                'currency_minor_unit': '2',
                'currency_name': 'Afghani',
                'currency_numeric_code': '971',
                'is_independent': 'Yes'
              },
              {
                'name': 'Albania',
                'name_fr': 'Albanie',
                'ISO3166-1-Alpha-2': 'AL',
                'ISO3166-1-Alpha-3': 'ALB',
                'ISO3166-1-numeric': '008',
                'ITU': 'ALB',
                'MARC': 'aa',
                'WMO': 'AB',
                'DS': 'AL',
                'Dial': '355',
                'FIFA': 'ALB',
                'FIPS': 'AL',
                'GAUL': '3',
                'IOC': 'ALB',
                'currency_alphabetic_code': 'ALL',
                'currency_country_name': 'ALBANIA',
                'currency_minor_unit': '2',
                'currency_name': 'Lek',
                'currency_numeric_code': '008',
                'is_independent': 'Yes'
              }]";

            return codes;
        }

    }
}
