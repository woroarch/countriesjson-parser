﻿using ConsoleParser.Repository;
using ConsoleParserTest.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace ConsoleParserTest
{
    [TestCaseOrderer("ConsoleParserTest.Extensions.ParserRepositoryTestCaseOrderer", "ConsoleParserTest")]
    public class ParserRepositoryFacts : IClassFixture<Extensions.DatabaseFixture>
    {
        private ITestOutputHelper m_outputHelper;
        private Extensions.DatabaseFixture m_fixture;
        public ParserRepositoryFacts(ITestOutputHelper output, Extensions.DatabaseFixture fixture)
        {
            m_outputHelper = output;
            m_fixture = fixture;
        }


        [Order(1)]
        [Fact]
        public void CreateTables()
        {
            var threadName = System.Threading.Thread.CurrentThread.ManagedThreadId;
            var appartment = System.Threading.Thread.CurrentThread.GetApartmentState().ToString();
            m_outputHelper.WriteLine("Thread name: {0}, \n Thread appartment: {1}", threadName, appartment);

            //Assert.False(m_fixture.Repository.Database.Exists());

           /*  m_fixture.Repository.Database.CreateIfNotExists();
            if (m_fixture.Repository.Database.Connection.State == ConnectionState.Closed)
                m_fixture.Repository.Database.Connection.Open();*/
           if (m_fixture.Repository.Database.Exists())
            {
                m_outputHelper.WriteLine("Database exists!");
                m_fixture.Repository.Database.Delete();
               
               
            }
           // else*/
            
                m_outputHelper.WriteLine("creating Database");
                m_fixture.Repository.Database.Create();
            

            

           // var tblCollection = m_fixture.Repository.Database.Connection.GetSchema("tables");

            //ShowDataTable(tblCollection);

            //var collection = ((IListSource)tblCollection).GetList() ;
            //m_fixture.Repository.Database.Connection.Close();

            Assert.True(m_fixture.Repository.Database.Exists());

            //DeleteTableTables();
        }

        private void ShowDataTable(DataTable table, Int32 length)
        {
            var sb = new StringBuilder();
            foreach (DataColumn col in table.Columns)
            {
                sb.AppendFormat("{0,-" + length + "}", col.ColumnName);
            }
            sb.AppendLine();

            foreach (DataRow row in table.Rows)
            {
                foreach (DataColumn col in table.Columns)
                {
                    if (col.DataType.Equals(typeof(DateTime)))
                        sb.AppendFormat("{0,-" + length + ":d}", row[col]);
                    else if (col.DataType.Equals(typeof(Decimal)))
                        sb.AppendFormat("{0,-" + length + ":C}", row[col]);
                    else
                        sb.AppendFormat("{0,-" + length + "}", row[col]);
                }
                sb.AppendLine();
            }

            m_outputHelper.WriteLine(sb.ToString());
        }

        private void ShowDataTable(DataTable table)
        {
            ShowDataTable(table, 14);
        }


        //[Order(2)]
       // [Fact]
        public void DeleteTableTables()
        {

            var threadName = System.Threading.Thread.CurrentThread.ManagedThreadId;
            var appartment = System.Threading.Thread.CurrentThread.GetApartmentState().ToString();
            m_outputHelper.WriteLine("Thread name: {0}, \n Thread appartment: {1}", threadName, appartment);
            
            /* if (m_fixture.Repository.Database.Connection.State == ConnectionState.Closed)
                m_fixture.Repository.Database.Connection.Open();*/

            System.Threading.Thread.Sleep(60000);
            try
            {
                if (m_fixture.Repository.Database.Exists())
                    m_fixture.Repository.Database.Delete();
            }
            catch{}

            Assert.False(m_fixture.Repository.Database.Exists());
        }

    }

    public class ParserRepositoryMockFacts
    {
        private ITestOutputHelper m_outputHelper;
        public ParserRepositoryMockFacts(ITestOutputHelper output)
        {
            m_outputHelper = output;
        }

    }
}
