﻿using ConsoleParser.Models;
using System;
using System.Collections.Generic;
using System.Linq;


namespace ConsoleParser.Utilities
{
    public  static class Helpers
    {

        public static IEnumerable<string> GetContinentsName() {

            var continentsName = Enum.GetNames(typeof(Continents)).OrderBy(s => s);

            foreach (var name in continentsName)
            {
                if (name.Contains('_'))
                   yield return name.Replace('_', ' ');
                else
                    yield return name;
            }
                
        }
    }
}
