﻿using ConsoleParser.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleParser
{
    class Program
    {
        static void Main(string[] args)
        {
            var spinner = new Spinner();
            var svc = new ParserService();

            Console.WriteLine("Press CTRL-C to exit.");

            LoadRegionsAndCountriesFromFileIntoRepository(Console.Out, spinner, svc);
                       
            Console.ReadKey(false);

        }

        private static void LoadCountriesFromFileIntoRepository(TextWriter textWriter, Spinner spinner, ParserService service)
        {
            spinner.StartSpinningAsync();
            System.Threading.Thread.Sleep(2000);
            textWriter.WriteLine("Parsing file country-codes.json.");
            System.Threading.Thread.Sleep(2000);
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data", "country-codes.json");
            textWriter.WriteLine("Constructing country objects.");
            var countries = ParserService.ParseCountriesJson(File.ReadAllText(path));
            System.Threading.Thread.Sleep(1500);

            textWriter.WriteLine("Persisting Country objects to repository");
            service.AddCountries(countries);
            textWriter.WriteLine("Done persisting Country objects to repository!");

            spinner.StopSpinning();

            textWriter.WriteLine("Press any key to exit. :)");
        }

        private static void LoadRegionsAndCountriesFromFileIntoRepository(TextWriter textWriter, Spinner spinner, ParserService service)
        {
            textWriter.WriteLine("Loading file countries.geojson.");
            spinner.StartSpinningAsync();
            var path = "";
            Task.Run(() =>
            {
                path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data", "countries.geojson");
                return File.ReadAllText(path);

            }).ContinueWith(async (regionJson) =>
            {
                await spinner.StartSpinningAsync();

                System.Threading.Thread.Sleep(2000);
                textWriter.WriteLine("Parsing file countries.geojson.");
                System.Threading.Thread.Sleep(2000);
                textWriter.WriteLine("Constructing region objects.");

                var parsedRegionJson = await regionJson;
                var regions = ParserService.ParseRegionJson(parsedRegionJson);
               
                
                System.Threading.Thread.Sleep(1500);
                textWriter.WriteLine("Persisting regions objects to repository");
                service.AddRegions(regions);

                LoadCountriesFromFileIntoRepository(textWriter, spinner, service);

                service.ParseSaveRegionJsonWithCountries(parsedRegionJson);
            });            
                    
        }
    }
}
