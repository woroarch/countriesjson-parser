﻿/*using System;*/
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Common;
using MySql.Data.Entity;
using ConsoleParser.Models;

//[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("ConsoleParserTest")]
namespace ConsoleParser.Repository
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public partial class ParserRepository : DbContext
    {
        
        public ParserRepository()
            : base(nameOrConnectionString: "MySqlContext")
        { Database.CreateIfNotExists(); }

        internal ParserRepository(string connectionString)
            : base(nameOrConnectionString: connectionString)
        { }

        internal ParserRepository(DbConnection connection, bool contextOwnsConnection) : base(connection, contextOwnsConnection){ }

        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<Country> Countries { get; set; }


        
    
    }
}
