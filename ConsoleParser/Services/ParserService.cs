﻿using ConsoleParser.Models;
using ConsoleParser.Repository;
using GeoJSON.Net.Feature;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleParser.Services
{
    public class ParserService
    {
        private ParserRepository parserRepository;
        private bool m_detectchanges;
        private bool m_validateSve;


        public ParserService(): this(new ParserRepository())
        {
            //to be verify
            if(parserRepository != null && parserRepository.Database != null && parserRepository.Database.Connection != null)
            {
                parserRepository.Database.Connection.Close();
                parserRepository.Dispose();
            }
            
        }


        public ParserService(ParserRepository parserRepository)
        {
            // TODO: Complete member initialization
            this.parserRepository = parserRepository;
        }

        public void AddRegion(Models.Region region)
        {
            using(parserRepository = new ParserRepository())
            {
                parserRepository.Database.UseTransaction(null);
                using (var ctx = parserRepository.Database.BeginTransaction())
                {
                    parserRepository.Regions.Add(region);
                    SaveChanges(ctx);
                }
            }
        }

       

        private void SaveChanges(System.Data.Entity.DbContextTransaction ctx = null)
        {
            try
            {
                parserRepository.SaveChanges();
                if (ctx != null)
                    ctx.Commit();
            }
            catch(Exception )
            {
                //discard changes
                if (ctx != null)
                {
                    ctx.Rollback();
                    ctx.UnderlyingTransaction.Dispose();
                    ctx.Dispose();
                }
                    
                parserRepository.Database.Connection.Close();
                parserRepository.Dispose();
                parserRepository = null;
            }
        }

        public void AddCountry(Models.Country country)
        {
            using (parserRepository = new ParserRepository())
            {
                parserRepository.Countries.Add(country);
                SaveChanges();
            }
        }

        public void AddCountries(IEnumerable<Models.Country> countries)
        {
            using (parserRepository = new ParserRepository())
            {
                parserRepository.Database.UseTransaction(null);
                using (var ctx =  parserRepository.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                {
                    PrepareContextBulkInser();

                    parserRepository.Countries.AddRange(countries);
                    SaveChanges(ctx);
                }
                //RevertPrepareContextBulkInser();
            }
        }

        private void PrepareContextBulkInser()
        {

            m_detectchanges = parserRepository.Configuration.AutoDetectChangesEnabled;
            m_validateSve = parserRepository.Configuration.ValidateOnSaveEnabled;

            parserRepository.Configuration.AutoDetectChangesEnabled = false;
            parserRepository.Configuration.ValidateOnSaveEnabled = false;
            
        }

        private void RevertPrepareContextBulkInser()
        {
            parserRepository.Configuration.AutoDetectChangesEnabled = m_detectchanges;
            parserRepository.Configuration.ValidateOnSaveEnabled = m_validateSve;
        }

        public void AddRegions(IEnumerable<Models.Region> regions)
        {
            using (parserRepository = new ParserRepository())
            {
                parserRepository.Database.UseTransaction(null);
                using (var ctx = parserRepository.Database.BeginTransaction())
                {
                    PrepareContextBulkInser();

                    parserRepository.Regions.AddRange(regions);
                    SaveChanges(ctx);
                }
                    //RevertPrepareContextBulkInser();
                
            }
        }

        

        public static IEnumerable<Region> ParseRegionJson(string jsonSource)
        {
            List<Region> regions = null;
            var featureCollection = JsonConvert.DeserializeObject<FeatureCollection>(jsonSource,
                            new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            if(featureCollection != null && featureCollection.Features.Count > 0)
            {
                regions = new List<Region>();
                var features = featureCollection.Features;

                foreach(var feature in features)
                {
                    var continent = feature.Properties["continent"] as string;
                    var region = feature.Properties["subregion"] as string;
                    
                    if(continent != null && region != null)
                    {
                        if (continent.Contains(' '))
                        {
                            int idx;
                            if((idx = continent.IndexOf('(')) != -1)
                            {
                                continent = continent.Remove(idx);
                            }
                            continent = continent.Trim().Replace(' ', '_'); 
                        }
                        regions.Add(new Region { Name = region, Continent = (Continents)Enum.Parse(typeof(Continents), continent, true) });
                    }
                }
            }
            return regions.GroupBy(a => a.Name)
                   .Select(g => g.First())
                   .ToList(); 
        }

        public  static IEnumerable<Country> ParseCountriesJson(string jsonSource)
        {
            var countries = JsonConvert.DeserializeObject<List<Country>>(jsonSource, new JsonSerializerSettings() { 
                 
            });

            return countries;
        }

        internal void ParseSaveRegionJsonWithCountries(string parsedRegionJson)
        {
            List<Region> regions = null;
            var featureCollection = JsonConvert.DeserializeObject<FeatureCollection>(parsedRegionJson,
                            new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            if (featureCollection != null && featureCollection.Features.Count > 0)
            {
                regions = new List<Region>();
                var features = featureCollection.Features;

               
                foreach (var feature in features)
                {
                    var continent = feature.Properties["continent"] as string;
                    var region = feature.Properties["subregion"] as string;
                    var isoAlpha3 = feature.Properties["adm0_a3"] as string;
                    if (continent != null && region != null)
                    {
                        if (continent.Contains(' '))
                        {
                            int idx;
                            if ((idx = continent.IndexOf('(')) != -1)
                            {
                                continent = continent.Remove(idx);
                            }
                            continent = continent.Trim().Replace(' ', '_');
                        }
                        regions.Add(new Region { Name = region, Continent = (Continents)Enum.Parse(typeof(Continents), continent, true), CountryName= isoAlpha3 });
                    }
                }

                using(parserRepository = new ParserRepository())
                {
                    parserRepository.Database.UseTransaction(null);
                    using (var ctx = parserRepository.Database.BeginTransaction())
                    {
                        PrepareContextBulkInser();

                        var allRegions = LoadAllRegions();
                        var allCountries = LoadAllCountries();

                        foreach(var region in regions)
                        {
                            var regionEntity = FindRegion(region, allRegions);
                            var countryEntity = FindCountry(region.CountryName, allCountries);

                            if(regionEntity != null && countryEntity != null)
                            {
                                parserRepository.Entry<Country>(countryEntity).Reference<Region>(c => c.Region).CurrentValue = regionEntity;
                                System.Diagnostics.Debug.WriteLine("Country: {0}, \tRegion: {1},  \tContinent: {2}.", countryEntity.Name, regionEntity.Name, regionEntity.ContinentName);
                            }
                            
                                 
                        }
                        
                        SaveChanges(ctx);
                    }
                }
            }
        }

        private IEnumerable<Country> LoadAllCountries()
        {
            return parserRepository.Countries.ToList();
        }

        private IEnumerable<Region> LoadAllRegions()
        {
            return parserRepository.Regions.ToList();
        }

        private Country FindCountry(string countryNameToFind, IEnumerable<Country> countries)
        {
            Country country = null;

            if (parserRepository != null && parserRepository.Database != null && parserRepository.Database.Connection != null)
                country = countries.FirstOrDefault(c => StringComparer.InvariantCultureIgnoreCase.Compare(c.NameISOAlpha3, countryNameToFind) == 0);
            

            return country;
        }

        private Region FindRegion(Region region, IEnumerable<Region> regions)
        {
            Region foundRegion = null;
            
            if(parserRepository != null && parserRepository.Database != null && parserRepository.Database.Connection != null )
                 foundRegion = regions.FirstOrDefault(r => StringComparer.InvariantCultureIgnoreCase.Compare(r.Name, region.Name) == 0);
            

            return foundRegion;
        }
    }
}
