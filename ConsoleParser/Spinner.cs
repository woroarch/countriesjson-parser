﻿using System;
using System.Threading;

namespace ConsoleParser
{
    internal class Spinner
    {
        private int _currentAnimationFrame;
        private int m_spin = 0;

        public Spinner()
        {
            SpinnerAnimationFrames = new[]
                                     {
                                         '|',
                                         '/',
                                         '-',
                                         '\\'
                                     };
        }

        public char[] SpinnerAnimationFrames { get; set; }

        public void UpdateProgress()
        {
            // Store the current position of the cursor
            var originalX = Console.CursorLeft;
            var originalY = Console.CursorTop;

            // Write the next frame (character) in the spinner animation
            Console.Write(SpinnerAnimationFrames[_currentAnimationFrame]);

            // Keep looping around all the animation frames
            _currentAnimationFrame++;
            if (_currentAnimationFrame == SpinnerAnimationFrames.Length)
            {
                _currentAnimationFrame = 0;
            }

            // Restore cursor to original position
            Console.SetCursorPosition(originalX, originalY);
        }

        public void StartSpinning() {
            if (m_spin == 1)
                return;


            System.Threading.Interlocked.Exchange(ref m_spin, 1);

            Console.CursorVisible = false;

            while (m_spin == 1)
            {
                Thread.Sleep(100); // simulate some work being done
                UpdateProgress();
                if (m_spin == 0)
                {
                    Console.WriteLine("Exiting");
                    
                    break;
                }
            }

        }

        public void StopSpinning()
        {
            System.Threading.Thread.Sleep(5000);
            System.Threading.Interlocked.Exchange( ref m_spin, 0);
            Console.CursorVisible = true;
        }

        public System.Threading.Tasks.Task StartSpinningAsync()
        {
            return System.Threading.Tasks.Task.Run(() => StartSpinning());
        }
    }
}
