﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConsoleParser.Models
{
    [Table("regions")]
    public class Region
    {
        Continents m_continent;
        public Region()
        {
            Countries = new HashSet<Country>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        
        [MaxLength(150)]
        [Index("region_name", IsUnique=true )]
        public string Name { get; set; }

       
        [NotMapped]
        public Continents Continent {
            get { return m_continent; }
            set { m_continent = value; }
        }

        [Column("continent")]
        public string ContinentName {
            get { return Enum.GetName(typeof(Continents), m_continent).Replace('_', ' '); }
            set { 
                if(value.Contains(' '))
                {
                    value = value.Trim().Replace(' ', '_');
                    
                }
                m_continent = (Continents)Enum.Parse(typeof(Continents), value, true); 
            }
        }

        [InverseProperty("Region")]
        public virtual ICollection<Country> Countries { get; set; }

        //used for parsing only
        [NotMapped]
        public string CountryName { get; set; }
    }
}
