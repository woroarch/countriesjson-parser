﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ConsoleParser.Models
{
    [Table("countries")]
    public class Country
    {
        [Key]
        [Column("id")]
        [JsonIgnore]
        public int Id { get; set; }

        [Column("name" )]
        [Index(IsUnique = true)]
        [StringLength(150)]
        [JsonProperty(PropertyName="name")]
      public string Name {get;set;}

    //"name_fr": "Afghanistan",
        [Column("name_fr")]
        [JsonProperty(PropertyName="name_fr")]
        public string NameFrench{get;set;}

        [Column("ISO3166-1-Alpha-2")]
        [JsonProperty(PropertyName="ISO3166-1-Alpha-2")]
        public string NameISOAlpha2 {get;set;}

    //"ISO3166-1-Alpha-3": "AFG",
        [Column("ISO3166-1-Alpha-3")]
        [JsonProperty(PropertyName="ISO3166-1-Alpha-3")]
        public string NameISOAlpha3 {get;set;}
    
        
        //"ISO3166-1-numeric": "004",
        [Column("ISO3166-1-numeric")]
        [JsonProperty(PropertyName="ISO3166-1-numeric")]
        public int? NameISONumeric {get;set;}

    //"ITU": "AFG",
        public string ITU {get;set;}
    
        //"MARC": "af",
        public string MARC {get;set;}

    //"WMO": "AF",
        public string WMO {get;set;}

    //"DS": "AFG",
        public string DS {get;set;}

        //"Dial": "93", or "1-684"
        public string Dial{get;set;}
    
        //"FIFA": "AFG",
        public string FIFA {get;set;}

    //"FIPS": "AF",
        public string FIPS{get;set;}
    
        //"GAUL": "1",
        public string GAUL {get;set;}

    //"IOC": "AFG",
        public string IOC { get; set; }
    
        //"currency_alphabetic_code": "AFN",
        [Column("currency_alphabetic_code")]
        [JsonProperty(PropertyName="currency_alphabetic_code")]
        public string CurrencyAlphaCode{get;set;}

    //"currency_country_name": "AFGHANISTAN",
        [Column("currency_country_name")]
        [JsonProperty(PropertyName="currency_country_name")]
        public string CurrencyCountryName{get;set;}

    //"currency_minor_unit": "2",
        [Column("currency_minor_unit")]
        [JsonProperty(PropertyName = "currency_minor_unit")]
        public int? CurrencyUnit{get;set;}

    //"currency_name": "Afghani",
        [Column("currency_name")]
        [JsonProperty(PropertyName = "currency_name")]
        public string CurrencyName{get;set;}
    //"": "971",
        [Column("currency_numeric_code")]
        [JsonProperty(PropertyName = "currency_numeric_code")]
        public int? CurrencyCode{get;set;}
    
        //"is_independent": "Yes"
        [NotMapped]
        [JsonProperty(PropertyName = "is_independent")]
        public string is_independent {
            get { return IsIndependent.ToString(); }
            set {
                if (StringComparer.InvariantCultureIgnoreCase.Equals(value, "true") || StringComparer.InvariantCultureIgnoreCase.Equals(value, "yes"))
                    IsIndependent = true;
                else
                    IsIndependent = false;
            }
        }

        [Column("is_independent")]
        [JsonIgnore]
        public bool? IsIndependent {get;set;}        
        
        // region reference
        [ForeignKey("continent_id")]
        [JsonIgnore]
        public virtual Region Region { get; set; }

     [JsonProperty(NullValueHandling= NullValueHandling.Ignore)]
        public int? continent_id { get; set; }
    }
}
