﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleParser.Models
{
    public enum Continents
    {
        Africa = 1,
        Europe = 2,
        Asia = 4,
        North_America = 8,
        South_America = 16,
        Antarctica = 32,
        Australia = 64,
        Seven_seas = 128,
        Oceania = 256
    }    
}
